package com.ninjya.sequence;

import java.util.List;


/**
 * Created by SC_001 on 2017/1/24.
 * UUID生成提供者接口
 */
public interface SequenceService {
    /**
     * Sequence service handel list.
     *
     * @param count the count
     * @return the list
     */
    List<Long> sequenceMultiServiceHandel(int count);

    /**
     * Sequence service handel list.
     *
     * @return the Long
     */
    Long sequenceServiceHandel();
}
