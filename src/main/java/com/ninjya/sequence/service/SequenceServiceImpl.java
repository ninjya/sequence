package com.ninjya.sequence.service;

import com.ninjya.sequence.IdWorker;
import com.ninjya.sequence.SequenceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by zhangliang on 2017/1/24.
 * 全局自增ID生成service
 */
public class SequenceServiceImpl implements SequenceService {
    private static Logger logger = LoggerFactory.getLogger(SequenceServiceImpl.class);

    private Long workerId = 0L;
    private Long dataCenterId = 0L;

    /**
     * Sequence service handel list.
     * 生成全局自增ID，返回多个
     *
     * @param count 想要生成UUID的个数
     * @return the list
     */
    @Override
    public List<Long> sequenceMultiServiceHandel(int count) {
        logger.debug("produce id start!");
        IdWorker iw = new IdWorker(workerId,dataCenterId);
        List<Long> seqs = new ArrayList<Long>();
        for(int i = 0; i < count; i++) {
            seqs.add(iw.nextId());
        }
        return seqs;
    }

    /**
     * Sequence service handel list.
     * 生成全局自增ID，返回单个
     *
     * @return the Long
     */
    @Override
    public Long sequenceServiceHandel() {
        IdWorker iw = new IdWorker(workerId,dataCenterId);
        return iw.nextId();
    }

}
