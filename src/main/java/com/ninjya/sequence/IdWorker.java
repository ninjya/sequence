package com.ninjya.sequence;

/**
 * Created by SC_001 on 2017/1/20.
 */

/**
 * Snowflake
 */
public class IdWorker {
    /**
     * 2010-11-04 09:42:54
     */
    private final long twepoch = 1288834974657L;
    private final long workerIdBits = 5L;
    private final long datacenterIdBits = 5L;
    /**
     * max=31
     */
    private final long maxWorkerId = -1L ^ (-1L << workerIdBits);
    /**
     * max=31
     */
    private final long maxDatacenterId = -1L ^ (-1L << datacenterIdBits);
    private final long sequenceBits = 12L;
    /**
     * 右移12位
     */
    private final long workerIdShift = sequenceBits;
    /**
     * 右移17位
     */
    private final long datacenterIdShift = sequenceBits + workerIdBits;
    /**
     * 右移22位
     */
    private final long timestampLeftShift = sequenceBits + workerIdBits + datacenterIdBits;
    /**
     * 4095
     */
    private final long sequenceMask = -1L ^ (-1L << sequenceBits);
    private long workerId;
    private long dataCenterId;
    /**
     * 自增从0开始
     */
    private long sequence = 0L;
    private long lastTimestamp = -1L;


    /**
     * Instantiates a new Id worker.
     * validation workerId and dataCenterId.
     *
     * @param workerId     the worker id
     * @param dataCenterId the datacenter id
     */
    public IdWorker(long workerId, long dataCenterId) {
        if (workerId > maxWorkerId || workerId < 0) {
            throw new IllegalArgumentException(String.format("worker Id can't be greater than %d or less than 0", maxWorkerId));
        }
        if (dataCenterId > maxDatacenterId || dataCenterId < 0) {
            throw new IllegalArgumentException(String.format("datacenter Id can't be greater than %d or less than 0", maxDatacenterId));
        }
        this.workerId = workerId;
        this.dataCenterId = dataCenterId;
    }

    /**
     * Next id long.
     * create UUID
     *
     * @return the long
     */
    public synchronized long nextId() {
        long timestamp = timeGen();
        if (timestamp < lastTimestamp) {
            throw new RuntimeException(String.format("Clock moved backwards.  Refusing to generate id for %d milliseconds", lastTimestamp - timestamp));
        }
        if (lastTimestamp == timestamp) {
            /**
             * 从一开始累计到最大等待下一个毫秒
             */
            sequence = (sequence + 1) & sequenceMask;
            if (sequence == 0) {
                timestamp = tilNextMillis(lastTimestamp);
            }
        } else {
            sequence = 0L;
        }

        lastTimestamp = timestamp;
        /**
         * 减去一个基础时间然后右移22位，放入时间戳
         * 右移17位，放入dataCenterId
         * 右移12位，放入workId
         * 剩下的12位放入自增ID
         */
        return ((timestamp - twepoch) << timestampLeftShift)
                | (dataCenterId << datacenterIdShift)
                | (workerId << workerIdShift)
                | sequence;
    }


    /**
     * Til next millis long.
     * the next millis back
     *
     * @param lastTimestamp the last timestamp
     * @return the long
     */
    protected long tilNextMillis(long lastTimestamp) {
        long timestamp = timeGen();
        while (timestamp <= lastTimestamp) {
            timestamp = timeGen();
        }
        return timestamp;
    }

    protected long timeGen() {
        return System.currentTimeMillis();
    }

}